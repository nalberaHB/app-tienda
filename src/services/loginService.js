

const loginService = async (url, data) => {

    const response = await fetch(url,{
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-type': 'application/json'
        }
    });

    const json = await response.json();

    if(!response.ok){
        console.log('error');
    }

    return json;
}

export default loginService;