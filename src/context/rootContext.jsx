'use client'

import { createContext, useCallback, useMemo, useState } from "react";



export const rootContext = createContext({
    login: (token) => {},
    logout: () => {},
    isLogged: false,
    token: null,
});


const ContextProvider = ({ children }) => {

    const tokenInLocalStorage = typeof window !== 'undefined' ? window.localStorage.getItem('token') : false;

    const [token, setToken] = useState(
        tokenInLocalStorage === null ?
        null
        :
        JSON.parse(tokenInLocalStorage)
    );

    const login = useCallback(( token ) => {
        typeof window !== 'undefined' ? window.localStorage.setItem('token',JSON.stringify(token)) : false;
        setToken(token);
    },[]);

    const logout = useCallback(() => {
        typeof window !== 'undefined' ? window.localStorage.removeItem('token') : false;
        setToken(null);
    },[]);

    const value = useMemo(() => ({
        login,
        logout,
        token,
        isLogged: token !== null
    }),[token, login, logout]);

    return(
        <rootContext.Provider value={ value }> { children } </rootContext.Provider>
    )
} 

export default ContextProvider;