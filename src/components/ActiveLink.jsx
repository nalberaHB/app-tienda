'use client';

import Link from "next/link";

import { usePathname } from "next/navigation";//mos permite leer la ruta actual

const style = {
    color: 'red',
    textDecoration: 'underline'
}

const ActiveLink = ({text, href}) => {

    const pathName = usePathname();

    return(
        <Link href={ href } style={ pathName === href ? style : null } >
            { text }
        </Link>
    )
}

export default ActiveLink;