import ActiveLink from "./ActiveLink";

import styles from '@/components/NavrBar.module.css';

export const NavBar = () => {
    return(
        <nav className={styles['menu-container']}>
            <ActiveLink text="Home" href="/" />
            <ActiveLink text="Login" href="/login" />
            <ActiveLink text="Register" href="/register" />
        </nav>
    )
}

