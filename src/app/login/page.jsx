'use client';

import loginService from "@/services/loginService";

import { useContext } from "react";
import { rootContext } from "@/context/rootContext";
import { useRouter } from "next/navigation";

const LoginPage = () => {

    const { login } = useContext(rootContext);
    const router = useRouter();

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        const data = {
            username: e.target[0].value,
            password: e.target[1].value
        }

        const token = await loginService('https://fakestoreapi.com/auth/login', data);

        //console.log(token);

        login(token);

        return router.push('/');

    }

    return(
        <>
            <h2>Login</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="">User Name</label>
                    <input type="text" name="username" id="" placeholder="Input User Name"/>
                </div>
                <div>
                    <label htmlFor="">Password</label>
                    <input type="text" name="password" id="" placeholder="Input User Name"/>
                </div>
                <div>
                    <button>Login</button>
                </div>
            </form>
        </>
    )
}

export default LoginPage;