'use client'

import { useContext } from "react";
import { rootContext } from "@/context/rootContext";
import { useRouter } from "next/navigation";

export default function Home() {

  const {isLogged} = useContext(rootContext);
  const router = useRouter();

  console.log(isLogged);

  isLogged == false ? router.push('/login') : null;
  
  return (
    <main >
        <h2>Hola</h2>
    </main>
  );
}
