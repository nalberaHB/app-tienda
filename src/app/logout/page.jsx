'use client'

import { rootContext } from "@/context/rootContext";
import { useContext, useEffect } from "react";


const logoutPage = () => {
    const { logout} = useContext(rootContext);

    useEffect(() => {
        logout();
    },[])

    return(
        false
    )
}

export default logoutPage;