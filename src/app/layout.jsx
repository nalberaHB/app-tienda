import { Inter } from "next/font/google";
//import "./globals.css";
import { NavBar } from "@/components/NavBar";
import ContextProvider from "@/context/rootContext";
const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Store App",
  description: "App",
  authors: [
    { name: 'Nelson Albera', url: 'http://gitlab....'}
  ]

};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <NavBar />
        <ContextProvider>
            {children}
        </ContextProvider>

      </body>
    </html>
  );
}
